#include<stdio.h>
#include<math.h>
int power(int x, unsigned int y)
{
    if (y == 0)
        return 1;
    else if (y%2 == 0)
        return power(x, y/2)*power(x, y/2);
    else
        return x*power(x, y/2)*power(x, y/2);
}

int gcd(int a, int b)
{
    if (b == 0)
        return a;
    return gcd(b, a % b);
}

int is_prime(unsigned int n)
{
int i;
for(i=2;i<n;i++)
{
if(n%i==0)
return 0;
}
return 1;
}

double FV(double rate, unsigned int nperiods, double PV)
{
    return PV * pow((1+rate),nperiods);
}

void factors(int num)
{
    int i;
    printf("%d's Factors are: \n",num);
    for(i=1; i <= num; ++i)
    {
        if (num%i == 0)
        {
            printf("%d ",i);
        }
    }
}

void primeFactor(int n)
{
    int i;
    printf("\nPrime Factors of %d are: ",n);
    while (n%2 == 0)
    {
        printf("%d ", 2);
        n = n/2;
    }
      for (i = 3; i <= sqrt(n); i = i+2)
    {
        while (n%i == 0)
        {
            printf("%d ", i);
            n = n/i;
        }
    }
     if (n > 2)
        printf ("%d ", n);
}

void oddEven(int oe)
{
     if(oe % 2 == 0)
        printf("%d is even. \n",oe);
    else
        printf("%d is odd. \n",oe);
}

void LCM(int n1, int n2, int n3)
{
    int minMult;
    if(n1>n2 && n1>n3)
        minMult=n1;
    else if(n2>n1 && n2>n3)
        minMult=n2;
    else
        minMult=n3;
    while(1)
    {
        if( minMult%n1==0 && minMult%n2==0 && minMult%n3==0)
        {
            printf("The LCM of %d and %d  and %d is %d \n", n1, n2,n3,minMult);
            break;
        }
        ++minMult;
    }
}

void FactorialNumber(unsigned int num)
{
    long long int factorial=1;
    while(num!=1)
    {
        factorial*=num;
        num--;
    }
    printf("%lld \n",factorial);
}

int fact(int x)
{
    int i=1;
    while(x!=0)
    {
        i=i*x;
        x--;
    }
    return i;
}

void combo()
{
    int n,r,ncr;
    printf("Enter N and R values: ");
    scanf("%d %d",&n,&r);
    ncr=fact(n)/(fact(r)*fact(n-r));
    printf("The NCR of %d and %d is %d \n",n,r,ncr);
}


int per_fact(int n)
{
    if (n <= 1)
        return 1;
    return n*per_fact(n-1);
}

int nPr(int n, int r)
{
    return per_fact(n)/per_fact(n-r);
}

void permutations()
{
    int n, r;
    printf("Enter n: \n");
    scanf("%d", &n);
    printf("Enter r: \n");
    scanf("%d", &r);
    printf("%dP%d is %d \n", n, r, nPr(n, r));
}

void fibo(int n)
{
    int f1 = 0, f2 = 1, i;
    if (n < 1)
        return;
    for (i = 1; i <= n; i++)
    {
        printf("%d ", f1);
        int next = f1 + f2;
        f1 = f2;
        f2 = next;
    }
}


  int main()
{
    int x = 2,p, prim_num=480,od_ev;
    unsigned int y = 3, n,fact_num=18;
    int a = 98, b = 56,num,n1,n2,n3;
    printf("Power Function: %d \n", power(x, y));
    printf("GCD of %d and %d is %d \n", a, b, gcd(a, b));
    printf("(Checking for prime)Enter a number : ");
    scanf("%d",&n);
    p=is_prime(n);
    if(p==1)
        printf("%d is prime\n",n);
    else
        printf("%d is not prime\n",n);
    printf("Future Value is: %f \n",FV(5,3,1000));
    printf("Enter a positive integer to find its factors: ");
    scanf("%d",&num);
    factors(num);
    primeFactor(prim_num);
    printf("\nEnter an integer: ");
    scanf("%d", &od_ev);
    oddEven(od_ev);
    printf("\nEnter three positive integers: ");
    scanf("%d %d %d", &n1, &n2, &n3);
    LCM(n1,n2,n3);
    FactorialNumber(fact_num);
    combo();
    permutations();
    fibo(10);
    return 0;
}






