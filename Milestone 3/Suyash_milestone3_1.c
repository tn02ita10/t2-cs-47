#include <stdio.h>

int main()
{
  int arr[10], sch, i, n, flag=0;
  printf("Enter total number of elements for the array(max 10)\n");
  scanf("%d",&n);
  printf("Enter elements\n",n);
  for (i=0;i<n;i++)
    scanf("%d",&arr[i]);
  printf("\nEnter the number to search\n");
  scanf("%d", &sch);
  for (i=0;i<n;i++)
  {
    if (arr[i]==sch)
    {
      printf("%d is at index %d.\n", sch, i+1);
      flag=1;
      break;
    }
  }
  if (flag==0)
    printf("%d is not present\n", sch);
  return 0;
}
