#include &lt;stdio.h&gt;

int main()
{
int array[100], S, c, a;

printf(&quot;Enter number of elements in array\n&quot;);
scanf(&quot;%d&quot;, &amp;a);

printf(&quot;Enter %d integer(s)\n&quot;, a);

for (c = 0; c &lt; a; c++)
scanf(&quot;%d&quot;, &amp;array[c]);

printf(&quot;Enter the number to be searched \n&quot;);
scanf(&quot;%d&quot;, &amp;S);

for (c = 0; c &lt; a; c++)
{
if (array[c] == S) /* If required element is found */
{
printf(&quot;%d is present at location %d.\n&quot;, S, c+1);
break;
}
}
if (c == a)
printf(&quot;%d isn&#39;t present in the array.\n&quot;, S);

return 0;
}